# About

It's an imaginary e-commerce of products based on fictional character Domo (NHK).

Made with [Spree](https://spreecommerce.com/) &hearts;

# Domo 

Domo is the official mascot of Japan's public broadcaster NHK, appearing in several 30-second stop-motion interstitial sketches shown as station identification during shows.

https://en.wikipedia.org/wiki/Domo_(NHK)

# Live Demo

https://domo-arigatou.herokuapp.com/